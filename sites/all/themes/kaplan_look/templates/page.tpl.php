<?php

?>
<div id="header">
  <div id="container">
    <div class="header">
      <?php if ($logo): ?>
      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
        <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
      </a>
      <?php endif; ?>

      <?php if ($site_name): ?>
        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><h1 id="site-name"><?php print $site_name; ?></h1></a>
      <?php endif; ?>

    </div>
  </div>
</div>
<div id="container">

  <div id="main-content">
    <?php if ($messages): ?>
    <div id="messages"><div class="section clearfix">
      <?php print $messages; ?>
    </div></div> <!-- /.section, /#messages -->
    <?php endif; ?>

    <?php if ($breadcrumb): ?>
      <div id="breadcrumb"><?php print $breadcrumb; ?></div>
    <?php endif; ?>

    <?php if ($title): ?>
      <h2 class="title" id="page-title">
        <?php print $title; ?>
      </h2>
    <?php endif; ?>

    <div id="content">
      <?php print render($page['content']); ?>
    </div>

    <?php if ($page['sidebar']): ?>
      <div id="sidebar" class="column sidebar">
        <?php print render($page['sidebar']); ?>
      </div> <!-- /#sidebar -->
    <?php endif; ?>
  </div>

  <?php if ($page['footer']): ?>
  <div id="footer" class="clearfix">
    <?php print render($page['footer']); ?>
  </div> <!-- /#footer -->
  <?php endif; ?>

</div>
