/**
 * @file
 * Adding drupal variables to js - this runs only on the kaplan-test page
 */

(function ($, Drupal, window, document, undefined) {

  // To understand behaviors, see https://drupal.org/node/756722#behaviors
  Drupal.behaviors.kaplanite = {
  attach: function(context, settings) {
/*############# Insert Code Below ###############*/

    var tipper = Drupal.settings.kaplanite.kaplankey;

    if(tipper=='tooltipper'){

      var message = $('div.field-name-kaplan-message-field > .field-items > .field-item').html();
      $('div.field-name-kaplan-message-field').hide();

      $('div.field-name-kaplan-body-field').qtip({ // Grab body field and apply tooltip
        content: {
          text: message
        },
        style: {
          classes: 'qtip-light qtip-shadow qtip-rounded',
          width: 200
        }
      });
    }



/*############# End Code Here ###############*/
    }
  }
})(jQuery, Drupal, this, this.document);
