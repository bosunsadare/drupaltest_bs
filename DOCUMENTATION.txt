This Documentation mainly covers what you need to do to get my Kaplan Test code installed and ready to view.

=================================================================

INSTALLATION
============
1. Download the test code from the repository.
2. Place it on a suitable webserver (LAMP, or anything that you would normally install fresh Drupal on)
3. Create a MySQL database and remember the name and password for later. The database should be empty
4. Type your folder root (http://yourdomain/) in the URL
5. You should see the normal Drupal installation page with an extra installation profile option "Kaplan"
6. Select the Kaplan option and continue
7. English is default on the next step so continue
8. Enter the name, user and password for the MySQL database your created, then continue
9. After the progress bar is complete, fill in your required details on the next page and continue
10. Your site should be installed now. Click "Visit your new site"

By now, you already have a functional site with necessary theme and modules installed and enabled, and the 3 nodes already created. You can click on "Kaplan Test" to see the qtip effect on the page. This effect will not be the case on other pages.

OTHER CONFIGURATIONS
====================
1. If mod-rewrite is enabled on your Apache install, then go to /admin/config/search/clean-urls and enable (if not already enabled)
2. You may want to change the pattern in URL alias settings for the kaplan content type to [node:title] (for this, go to /admin/config/search/path/patterns)

If any issues, I'm available to investigate and answer your questions. I hope for the best. Thanks.